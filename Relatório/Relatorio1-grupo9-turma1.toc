\select@language {brazil}
\contentsline {section}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{2}
\contentsline {section}{\numberline {2}Procedimentos}{3}
\contentsline {subsection}{\numberline {2.1}M\IeC {\'e}todo de Jacobi no Paradigma Sequencial}{3}
\contentsline {subsection}{\numberline {2.2}M\IeC {\'e}todo de Jacobi no Paradigma Paralelo}{3}
\contentsline {subsection}{\numberline {2.3}Compilando e Executando o C\IeC {\'o}digo}{3}
\contentsline {subsection}{\numberline {2.4}Considera\IeC {\c c}\IeC {\~o}es Importantes}{4}
\contentsline {section}{\numberline {3}Resultados}{5}
\contentsline {section}{\numberline {4}Considera\IeC {\c c}\IeC {\~o}es Finais e Conclus\IeC {\~o}es}{7}
\contentsline {section}{\numberline {5}Refer\IeC {\^e}ncias Bibliogr\IeC {\'a}ficas}{8}
