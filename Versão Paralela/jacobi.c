/*                                         JACOBI-RICHARDSON (PARALELO)                                                  */
/*                                         Ederson Tyiuji Noya - 7656022                                                 */
/*                                           Alisson Mateus - 8066287                                                    */
/*                                                                                                                       */

#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<math.h>
#include"jacobi.h"
#include"get_data.h"

//barreira para sincronizar as threads
pthread_barrier_t barreira;

typedef struct
{
	int id;
	int ini;	//linha onde começa a ler
	int fim;	//linha onde termina a leitura
}thread_arg;

/*************************************************************************************************************************/
/*************************************************** Jacobi-Richardson ***************************************************/
/*************************************************************************************************************************/
int jacobi_r(){
	int n_inter;
	int n_threads, i;
	int n_linhas = 250;	//Numero de linhas que cada thread deve trabalhar
	int cont;

	/** Opens file for write **/
    	if(!(file_out = fopen("Output_paralelo.txt", "w"))){
		printf("Erro de arquivo!\n");
		exit(EXIT_FAILURE);
	}

	flag_conv = 1;

	//criar a matriz A*
	matrixStar = alocMatrix();
	vetorStar  = alocVetor();

	//Calcula o numero de threads
	n_threads = J_ORDER / n_linhas;

	pthread_barrier_init(&barreira, NULL, n_threads);
	
	pthread_t tid[n_threads];
	thread_arg a[n_threads];

	cont = 0;
	//Cria as threads	
	for(i = 0; i < n_threads; i++){
		//na ultima thread verifica se ela tem que finalizar linhas que sobraram
		if(i == (n_threads - 1)){
			//divide linhas igualmente entre todas as threads
			if((J_ORDER % n_linhas) == 0){
				a[i].id  = i;
				a[i].ini = cont;				
				a[i].fim = cont + (n_linhas - 1);
				pthread_create(&(tid[i]), NULL, thread, (void *)&(a[i]));
			}
			else{
				//preciso delegar as linhas restantes a ultima thread				
				a[i].id  = i;
				a[i].ini = cont;
				a[i].fim = cont + ((n_linhas -1) + (J_ORDER % n_linhas));
				pthread_create(&(tid[i]), NULL, thread, (void *)&(a[i]));
			}
		}
   		a[i].id  = i;
		a[i].ini = cont;				//linha inicial;
		a[i].fim = cont + (n_linhas - 1);	//linha final;
     	pthread_create(&(tid[i]), NULL, thread, (void *)&(a[i]));
		
		cont += n_linhas;
	}

				/*                           */
				/*   executando as threads   */
				/*                           */

	//Espera até que todas as threads terminem de executar
	pthread_barrier_wait(&barreira);

	//Espera as threads terminarem...
    	for(i = 0; i < n_threads; i++){
        	pthread_join(tid[i], NULL);
    	}

	//escreve A* e V* no arquivo
	write_mat_vet_arq(file_out);	

	//verificar convergencia
	if(flag_conv == 0){
		printf("\nCriterio de convergencia nao satisfeito!\n");
		return -1;
	}

	n_inter = iter();

	fclose(file_out);

	return n_inter;


}

/*************************************************************************************************************************/
/******************************************************* Threads *********************************************************/
/*************************************************************************************************************************/
void *thread(void *vargp){
    	int i, j, cont;	//cont é a coluna com o valor da diagonal
	thread_arg *a = (thread_arg *) vargp;	
	
	cont = a->ini;
	for(i = (a->ini); i < (a->fim); i++){
		for(j = 0; j < J_ORDER; j++){
			if(i == j){
				matrixStar[i][j] = 0;
			}
			else{
				matrixStar[i][j] = ((matrixA[i][j]) / (matrixA[i][cont]));
			}
		}
		vetorStar[i] = (vetorA[i]) / (matrixA[i][cont]);
		cont++;
	}
    	//Espera até que todas as threads tenham acabado de executar
	pthread_barrier_wait(&barreira);

	//como todas as threads já terminaram de executar, a matriz A* ja esta pronta
	float soma = 0;
	for(i = (a->ini); i < (a->fim); i++){
		for(j = 0; j < J_ORDER; j++){		
			soma += matrixStar[i][j];
		}
		if(soma > 1){
			flag_conv = 0;
			printf("soma: %.2f\n", soma);	
		}		
		soma = 0;
	}
	//outra flag ja pode ter setado para 0, dai não pode mudar para 1
	if(flag_conv != 0){
		flag_conv = 1;
	}

    	pthread_exit((void *)NULL);
}

//retorna  o numero de interaçoes efetuadas do programa
int iter(){
	float **x;
	float **dif;
	float soma, max_dif, max_abs_x, mr;
	int i, j, w, flag_loop;
	int k;			//contador de linhas
	int cont_inter = 0;	//Conta o numero de iteraçoes
	int size_x;
	int size_dif;


	x = (float**)malloc(sizeof(float*)*1);
	x[0] = (float*)malloc(sizeof(float)*J_ORDER);

	dif = (float**)malloc(sizeof(float*)*1);
	dif[0] = (float*)malloc(sizeof(float)*J_ORDER);


	for(i = 0; i < J_ORDER; i++){
		x[0][i] = vetorStar[i];
	}

	for(i = 0; i < J_ORDER; i++){
		dif[0][i] = 0;
	}

	size_x = 1;
	size_dif = 1;
	k = 1; //começa da segunda linha
	flag_loop = 1;
	do{
		cont_inter++;

	x = (float**)realloc(x, (size_x+1) * sizeof(float*));
	x[size_x] = (float*)malloc(sizeof(float)*J_ORDER);
	size_x++;

	dif = (float**)realloc(dif, (size_dif+1) * sizeof(float*));
	dif[size_dif] = (float*)malloc(sizeof(float)*J_ORDER);
	size_dif++;

		for(i = 0; i < J_ORDER; i++){
			soma = 0;
			for(j = 0; j < J_ORDER; j++){	
				soma += matrixStar[i][j] * x[k-1][j];				
			}
			soma = soma*-1;	//conserta o sinal de + p/ - e de - p/ +
			x[k][i] = vetorStar[i] + soma;

			dif[k][i] = fabs((x[k][i]) - (x[k-1][i]));
		}

		/*** Calcula valor maximo de x ***/
		w = 0;
		max_abs_x = fabs(x[k][w]);
		for(w = 1; w < J_ORDER; w++){
			if(fabs(x[k][w]) > max_abs_x){
				max_abs_x = fabs(x[k][w]);
			}
		}

		/*** Calcula valor maximo de dif ***/
		w = 0;
		max_dif = fabs(dif[k][w]);
		for(w = 1; w < J_ORDER; w++){
			if(dif[k][w] > max_dif){
				max_dif = dif[k][w];
			}
		}

/************************************************************/
/******************* Critério de parada *********************/
/************************************************************/
		mr = max_dif / max_abs_x;
		if(mr <= J_ERROR || cont_inter > J_ITE_MAX){
			flag_loop = 0;
		}

		k++;

	}while(flag_loop);

	//calcula comp_res para impressão (J_ROW_TEST)
	comp_res = 0;
	for(i = 0; i < J_ORDER; i++){
		comp_res += (matrixA[J_ROW_TEST][i] * x[(cont_inter - 1)][i]);
	}


	fprintf(file_out, "Matriz X[][]:\n");
	for(i = 0; i < (cont_inter); i++){
		for(j = 0; j < J_ORDER; j++){
			fprintf (file_out, "%.3f\t", x[i][j]);
		}
		fprintf(file_out, "\n");
	}
	
	fprintf(file_out, "Matriz dif[][]:\n");
	for(i = 0; i < (cont_inter); i++){
		for(j = 0; j < J_ORDER; j++){
			fprintf (file_out, "%.3f\t", dif[i][j]);
		}
		fprintf(file_out, "\n");
	}
		
	return cont_inter;
}

void write_mat_vet_arq(FILE *file_out){
	int i, j;
	fprintf(file_out, "Matriz A*[][]:\n");
	for(i = 0; i < J_ORDER; i++){
		for(j = 0; j < J_ORDER; j++){
			fprintf (file_out, "%.3f\t", matrixStar[i][j]);
		}
		fprintf(file_out, "\n");
	}
	
	fprintf(file_out, "Vetor B*[]:\n");
	for(i = 0; i < J_ORDER; i++){
		fprintf (file_out, "%.3f\t", vetorStar[i]);
	}
	fprintf(file_out, "\n");
}

