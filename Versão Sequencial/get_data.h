/*                                        JACOBI-RICHARDSON (SEQUENCIAL)                                                 */
/*                                        Ederson Tyiuji Noya - 7656022                                                  */
/*                                           Alisson Mateus - 8066287                                                    */
/*                                                                                                                       */

int n_order;
float comp_res;//resultado da ultima iteração para comparar no final(ROW_TEST)

int exec(char *arquivo);
float* alocVetor();
float** alocMatrix();
void matrix_free();
void vetor_free();
void load_settings(FILE *fp);
void fillsMatrixA(FILE *fp);
void fillsVetorA(FILE *fp);

