/*                                        JACOBI-RICHARDSON (SEQUENCIAL)                                                 */
/*                                        Ederson Tyiuji Noya - 7656022                                                  */
/*                                           Alisson Mateus - 8066287                                                    */
/*                                                                                                                       */

#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<math.h>
#include"jacobi.h"
#include"get_data.h"


typedef struct
{
	int id;
	int ini;	//linha onde começa a ler
	int fim;	//linha onde termina a leitura
}thread_arg;

/*************************************************************************************************************************/
/*************************************************** Jacobi-Richardson ***************************************************/
/*************************************************************************************************************************/
int jacobi_r(){
	int n_inter;
	
	/** Opens file for write **/
    	if(!(file_out = fopen("Output_sequencia.txt", "w"))){
		printf("Erro de arquivo!\n");
		exit(EXIT_FAILURE);
	}

	//criar a matriz A*
	matrixStar = alocMatrix();
	vetorStar  = alocVetor();

	createMatrixStar();

	//escreve A* e V* no arquivo
	write_mat_vet_arq(file_out);	

	//verificar convergencia
	if(!(ver_conv())){
		printf("\nCriterio de convergencia nao satisfeito!\n");
		return -1;
	}

	n_inter = iter();

	return n_inter;


}

void createMatrixStar(){
	int i, j, cont = 0;	//cont é a coluna com o valor da diagonal
	
	for(i = 0; i < J_ORDER; i++){
		for(j = 0; j < J_ORDER; j++){
			if(i == j){
				matrixStar[i][j] = 0;
			}
			else{
				matrixStar[i][j] = ((matrixA[i][j]) / (matrixA[i][cont]));
			}
		}
		vetorStar[i] = (vetorA[i]) / (matrixA[i][cont]);
		cont++;
	}	
}

int ver_conv(){
	int i, j;
	float soma = 0;
	for(i = 0; i < J_ORDER; i++){
		for(j = 0; j < J_ORDER; j++){		
			soma += matrixStar[i][j];
		}
		if(soma > 1){
			return 0;		
		}
		soma = 0;
	}
	return 1;
}
//retorna  o numero de interaçoes efetuadas do programa
int iter(){
	float **x;
	float **dif;
	float soma, max_dif, max_abs_x, mr;
	int i, j, w, flag_loop;
	int k;			//contador de linhas
	int cont_inter = 0;	//Conta o numero de iteraçoes
	int size_x;
	int size_dif;

	//Aloca a primeira posição da matriz x e da matriz dif
	x = (float**)malloc(sizeof(float*)*1);
	x[0] = (float*)malloc(sizeof(float)*J_ORDER);

	dif = (float**)malloc(sizeof(float*)*1);
	dif[0] = (float*)malloc(sizeof(float)*J_ORDER);

	//Preenche a primeira linha de X[][] e de dif[][]
	for(i = 0; i < J_ORDER; i++){
		x[0][i] = vetorStar[i];
	}
	for(i = 0; i < J_ORDER; i++){
		dif[0][i] = 0;
	}
	
	//variaveis de controle de qtd de linhas para realloc
	size_x = 1;
	size_dif = 1;
	k = 1; //começa da segunda linha
	flag_loop = 1;
	do{
		cont_inter++;

		x = (float**)realloc(x, (size_x+1) * sizeof(float*));
		x[size_x] = (float*)malloc(sizeof(float)*J_ORDER);
		size_x++;

		dif = (float**)realloc(dif, (size_dif+1) * sizeof(float*));
		dif[size_dif] = (float*)malloc(sizeof(float)*J_ORDER);
		size_dif++;
		//Preenche a linha da matriz x[][] e depois de dif[][]
		for(i = 0; i < J_ORDER; i++){
			soma = 0;
			for(j = 0; j < J_ORDER; j++){	
				soma += matrixStar[i][j] * x[k-1][j];				
			}
			soma = soma*-1;	//conserta o sinal de + p/ - e de - p/ +
			x[k][i] = vetorStar[i] + soma;

			dif[k][i] = fabs((x[k][i]) - (x[k-1][i]));
		}

		/*** Calcula valor maximo de x ***/
		w = 0;
		max_abs_x = fabs(x[k][w]);
		for(w = 1; w < J_ORDER; w++){
			if(fabs(x[k][w]) > max_abs_x){
				max_abs_x = fabs(x[k][w]);
			}
		}

		/*** Calcula valor maximo de dif ***/
		w = 0;
		max_dif = fabs(dif[k][w]);
		for(w = 1; w < J_ORDER; w++){
			if(dif[k][w] > max_dif){
				max_dif = dif[k][w];
			}
		}

/************************************************************/
/******************* Critério de parada *********************/
/************************************************************/
		mr = max_dif / max_abs_x;
		if(mr <= J_ERROR || cont_inter > J_ITE_MAX){
			flag_loop = 0;
		}

		k++;

	}while(flag_loop);

	//calcula comp_res para impressão (J_ROW_TEST)
	comp_res = 0;
	for(i = 0; i < J_ORDER; i++){
		comp_res += (matrixA[J_ROW_TEST][i] * x[(cont_inter - 1)][i]);
	}

	//Impressão no arquivo de saida a matrix X e matrix dif
	fprintf(file_out, "Matriz X[][]:\n");
	for(i = 0; i < (cont_inter); i++){
		for(j = 0; j < J_ORDER; j++){
			fprintf (file_out, "%.3f\t", x[i][j]);
		}
		fprintf(file_out, "\n");
	}
	
	fprintf(file_out, "Matriz dif[][]:\n");
	for(i = 0; i < (cont_inter); i++){
		for(j = 0; j < J_ORDER; j++){
			fprintf (file_out, "%.3f\t", dif[i][j]);
		}
		fprintf(file_out, "\n");
	}

	//libera memoria das matrizes
	for(i = 0; i < (cont_inter); i ++)
		free(x[i]);
	free(x);

	for(i = 0; i < (cont_inter); i ++)
		free(dif[i]);
	free(dif);

	return cont_inter;
}

//Escreve no arquivo de saida as matrizes A* e o vetor B*
void write_mat_vet_arq(FILE *file_out){
	int i, j;
	fprintf(file_out, "Matriz A*[][]:\n");
	for(i = 0; i < J_ORDER; i++){
		for(j = 0; j < J_ORDER; j++){
			fprintf (file_out, "%.3f\t", matrixStar[i][j]);
		}
		fprintf(file_out, "\n");
	}
	
	fprintf(file_out, "Vetor B*[]:\n");
	for(i = 0; i < J_ORDER; i++){
		fprintf (file_out, "%.3f\t", vetorStar[i]);
	}
	fprintf(file_out, "\n");
}

