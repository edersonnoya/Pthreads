/*                                        JACOBI-RICHARDSON (SEQUENCIAL)                                                 */
/*                                        Ederson Tyiuji Noya - 7656022                                                  */
/*                                           Alisson Mateus - 8066287                                                    */
/*                                                                                                                       */

#include<stdio.h>
#include<stdlib.h>
#include <time.h>
#include"get_data.h"
#include"jacobi.h"


/*************************************************************************************************************************/
/********************************************************** Main *********************************************************/
/*************************************************************************************************************************/
int main(int argc,  char *argv[]){
	struct timeval t1, t2;		//variáveis de tempo inicial e final
    	double elapsedTime, time = 0;	//variável que conterá o tempo de execução
	int n_inter, i;
	
	//Program Output
	system("clear");
	printf("\nJACOBI-RICHARDSON (SEQUENCIAL)\n\n");

	printf("Processando...\n");

	gettimeofday(&t1, NULL);	//inicia o temporizador

	//abre o arquivo passado como argumento
	if(argv[1] == NULL){
		printf("\nNome do arquivo de leitura não foi inserido na entrada padrão!\nFavor inserir o nome do arquivo de leitura.\n\n");
		return 1;
	}
	//Executa o método de jacobi 10x para a mesma matriz
	for(i = 0; i < 10; i++){
		n_inter = exec(argv[1]);

		gettimeofday(&t2, NULL);
		elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
		elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
		time += elapsedTime;
	}

   	/** Program output **/
	printf("\n\n-----------------------------------------\n");
	printf("Informacoes do arquivo:\n");
    	printf("J_ORDER:\t%d\nJ_ROW_TEST:\t%d\nJ_ERROR:\t%f\nJ_ITE_MAX:\t%d\n\n\n", J_ORDER, J_ROW_TEST, J_ERROR, J_ITE_MAX);
   	printf("Iterations:\t%d\n", n_inter);
    	printf("RowTest: %d => [%f] =? %f\n", J_ROW_TEST, comp_res, vetorA[J_ROW_TEST]);
	printf("Tempo medio de Execução: %fms.\n", time/10);	
	printf("Tempo total de Execução: %fms.\n", time);	
	printf("-----------------------------------------\n");


    return 0;
}
