/*                                        JACOBI-RICHARDSON (SEQUENCIAL)                                                 */
/*                                        Ederson Tyiuji Noya - 7656022                                                  */
/*                                           Alisson Mateus - 8066287                                                    */
/*                                                                                                                       */

int J_ORDER, J_ROW_TEST, J_ITE_MAX;
float J_ERROR;
float **matrixA;
float *vetorA;
float **matrixStar;
float *vetorStar;
FILE *file_out;

int n_order_jac;

int jacobi_r();
void createMatrixStar();
void write_mat_vet_arq(FILE *file_out);
int ver_conv();
int iter();
void *thread(void *vargp);
