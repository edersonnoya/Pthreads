
/*                                        JACOBI-RICHARDSON (SEQUENCIAL)                                                 */
/*                                GRUPO:   Ederson Tyiuji Noya - 7656022                                                 */
/*                                           Alisson Mateus - 8066287                                                    */
/*                                                                                                                       */


Para rodar o programa:
	gcc -c jacobi.c
	gcc -c get_data.c
	gcc -c main.c
	gcc -o prog main.o get_data.o jacobi.o
	./prog "COLOCAR O NOME DO ARQUIVO QUE CONTÉM A MATRIZ AQUI!" //por exemplo ./prog matriz_teste.txt

Obs.:	(1) O arquivo contendo os dados e a matriz a ser calculada deve constar no mesmo diretório executavel.
		(2) O arquivo que contem os dados a serem processados deve estar no padrão definido na especificação do projeto.

